/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(10, 12);
        System.out.println(g);

        PathFinder pf = new PathFinder(g);

        int[][] adjMatrix = g.createAdjMatrix();
        int[][] distMatrix = pf.distMatrix;
        int[][] shortestPathsMatrix = pf.getShortestPaths();

        int diameter = pf.getLongestSimplePath();

        System.out.println("---ADJACENCY MATRIX---");
        printMatrix(adjMatrix);
        System.out.println("---DISTANCE MATRIX---");
        printMatrix(distMatrix);
        System.out.println("---SHORTEST PATHS---");
        printMatrix(shortestPathsMatrix);
        System.out.println("\n diameter: " + diameter);

    }

    /**
     * From given graph finds the diameter or shortest possible paths matrix using Floyd-Warshall algorithm.
     */
    class PathFinder {
        private final int nVert;

        public int[][] getDistMatrix() {
            return distMatrix;
        }

        private final int[][] distMatrix;

        /**
         * @param graph Graph to be processed.
         * @throws RuntimeException thrown when graph has vertices containing a loop to itself.
         */
        PathFinder(Graph graph) throws RuntimeException {
            if (graph == null) throw new RuntimeException("No graph given!");
            this.nVert = graph.getVertexCount();
            int[][] adjMatrix = graph.createAdjMatrix();
            try {
                this.distMatrix = createDistMatrix(adjMatrix);
            } catch (RuntimeException e) {
                throw new RuntimeException(e.getMessage());
            }
        }


        /**
         * Finds graphs diameter - longest path from all shortest paths.
         *
         * @return longest simple path.
         */
        public int getLongestSimplePath() {
            if (nVert <= 1) return nVert;
            int[][] shortestPaths = getShortestPaths();
            int longest = 0;
            for (int i = 0; i < shortestPaths.length; i++) {
                for (int j = 0; j < shortestPaths.length; j++) {
                    if (shortestPaths[i][j] > longest && i != j) {
                        longest = shortestPaths[i][j];
                    }
                }
            }
            return longest;
        }

        /**
         * Finds shortest possible paths between nodes using Floyd-Warshall algorithm.
         *
         * @return modified distance matrix with possible shortest paths.
         */
        public int[][] getShortestPaths() {
            int[][] matrix = new int[nVert][nVert];
            for (int i = 0; i < distMatrix.length; i++) {
                System.arraycopy(distMatrix[i], 0, matrix[i], 0, distMatrix[i].length);
            }

            if (nVert < 1) return matrix;
            for (int k = 0; k < nVert; k++) {
                for (int i = 0; i < nVert; i++) {
                    for (int j = 0; j < nVert; j++) {
                        if (i == j && matrix[i][j] != 0) {
                            throw new RuntimeException("Vertex v:" + i + "contains loops to itself!");
                        }
                        int newLength = matrix[i][k] + matrix[k][j];
                        if (matrix[i][j] > newLength) {
                            matrix[i][j] = newLength; // new path is shorter
                        }
                    }
                }
            }
            for (int i = 0; i < nVert; i++) {
                for (int j = 0; j < nVert; j++) {
                    if (i == j) {
                        matrix[i][j] = 0;
                    }
                }
            }
            return matrix;
        }

        /**
         * @param adjMatrix nested array containing 0 or 1 whether an edge between two nodes exits.
         * @return distance matrix where zeroes (no edge between vertices)
         * has been replaced by a large positive number.
         * @throws RuntimeException thrown when a loop from a vertex to itself has been found.
         */
        private int[][] createDistMatrix(int[][] adjMatrix) throws RuntimeException {
            int[][] distMatrix = new int[nVert][nVert];

            int INFINITY = 2 * nVert + 1;
            for (int i = 0; i < nVert; i++) {
                for (int j = 0; j < nVert; j++) {
                    if (i == j && adjMatrix[i][j] != 0) {
                        throw new RuntimeException("Vertex v: " + i + "contains loops to itself!");
                    }
                    if (adjMatrix[i][j] == 0 && i != j) {
                        distMatrix[i][j] = INFINITY;
                    } else if (i != j) {
                        distMatrix[i][j] = adjMatrix[i][j];
                    }
                }
            }
            return distMatrix;
        }
    }


    /**
     * Vertex representing an node in telephone network.
     */
    class Vertex {

        private final String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }


        @Override
        public String toString() {
            return id;
        }

        //for testing purposes
        public void setFirst(Arc arc) {
            this.first = arc;
        }

    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private final String id;
        private Vertex target;
        private Arc next;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        //for testing purposes
        public void setTarget(Vertex target) {
            this.target = target;
        }

    }

    /**
     * Undirected, weightless, acyclic graph.
     */
    class Graph {

        private final String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        //for testing purposes
        public Vertex getFirst() {
            return first;
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.id);
                    sb.append("->");
                    sb.append(a.target.id);
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * @param vid string id for result vertex.
         * @return new vertex that is also the new starting point for the graph.
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * @param aid  string id for result arc.
         * @param from arcs start node.
         * @param to   arcs end node.
         * @return arc between start vertex and end vertex.
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Starting from first given node in graph, count all vertices.
         *
         * @return Vertex count.
         */
        public int getVertexCount() {
            int c = 0;
            Vertex v = this.first;
            while (v != null) {
                c++;
                v = v.next;
            }
            return c;
        }
    }

    /**
     * Print matrix values line by line to the console.
     *
     * @param m 2x nested array containing integer values.
     */
    public void printMatrix(int[][] m) {
        if (m == null || m.length == 0) return;
        int b = 0;
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (m[i][j] > b) {
                    b = m[i][j];
                }
            }
        }
        int space = String.valueOf(b).length();

        StringBuilder sb = new StringBuilder();
        for (int[] ints : m) {
            for (int anInt : ints) {
                for (int i = 0; i < space - String.valueOf(anInt).length(); i++) {
                    sb.append(" ");
                }
                sb.append(anInt).append(" ");
            }
            System.out.println(sb);
            sb = new StringBuilder();
        }
    }

}

