import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Testklass.
 *
 * @author rvarbl
 */
public class GraphTaskTest {
    GraphTask gt = new GraphTask();
    GraphTask.Graph graph = gt.new Graph("TestGraph");

    @Test(expected = RuntimeException.class)
    public void testPathFinderEmptyGraph() {
        GraphTask.PathFinder pathFinder = gt.new PathFinder(null);
    }

    @Test(expected = RuntimeException.class)
    public void testPathFinderContainsLoops() {
        graph.createRandomSimpleGraph(4, 6);
        GraphTask.Vertex vertex = graph.getFirst();
        GraphTask.Arc arc = gt.new Arc("loop");
        arc.setTarget(vertex);
        vertex.setFirst(arc);

        gt.new PathFinder(graph);
    }


    @Test
    public void testPathFinderZeroNode() {
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);
        int diameter = pathFinder.getLongestSimplePath();
        assertEquals(0, diameter);

        graph.createRandomSimpleGraph(0, 0);
        GraphTask.PathFinder pathFinder2 = gt.new PathFinder(graph);
        int diameter2 = pathFinder2.getLongestSimplePath();
        assertEquals(0, diameter2);
    }

    @Test
    public void testPathFinderOneNode() {
        long start = System.currentTimeMillis();
        graph.createRandomSimpleGraph(1, 0);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);

        int diameter = pathFinder.getLongestSimplePath();
        assertEquals(diameter, 1);
        System.out.println("Diameter: " + diameter);
        gt.printMatrix(pathFinder.getShortestPaths());

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("TIME: " + timeElapsed + "ms");
    }

    @Test
    public void testPathFinderSixNodes() {
        long start = System.currentTimeMillis();
        graph.createRandomSimpleGraph(6, 9);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);

        int diameter = pathFinder.getLongestSimplePath();

        System.out.println(graph);
        System.out.println("Diameter: " + diameter);
        System.out.println("Shortest paths matrix");
        gt.printMatrix(pathFinder.getShortestPaths());

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("TIME: " + timeElapsed + "ms");
    }

    @Test
    public void testPathFinderTenNodes() {
        long start = System.currentTimeMillis();
        graph.createRandomSimpleGraph(10, 12);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);
        System.out.println(graph);
        int diameter = pathFinder.getLongestSimplePath();

        System.out.println("Diameter: " + diameter);
        System.out.println("Shortest paths matrix");
        gt.printMatrix(pathFinder.getShortestPaths());

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("TIME: " + timeElapsed + "ms");
    }

    @Test
    public void testPathFinderHundredNodes() {
        long start = System.currentTimeMillis();

        graph.createRandomSimpleGraph(100, 120);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);
        int diameter = pathFinder.getLongestSimplePath();

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("Diameter: " + diameter);
        System.out.println("TIME: " + timeElapsed + "ms");

    }

    @Test
    public void testPathFinderThousandNodes() {
        long start = System.currentTimeMillis();

        graph.createRandomSimpleGraph(1000, 1200);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);
        int diameter = pathFinder.getLongestSimplePath();

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("Diameter: " + diameter);
        System.out.println("TIME: " + timeElapsed + "ms");
    }

    @Test
    public void testPathFinderTwoThousandNodes() {
        long start = System.currentTimeMillis();

        graph.createRandomSimpleGraph(2500, 2800);
        GraphTask.PathFinder pathFinder = gt.new PathFinder(graph);
        int diameter = pathFinder.getLongestSimplePath();

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        System.out.println("Diameter: " + diameter);
        System.out.println("TIME: " + timeElapsed + "ms");
    }

}

