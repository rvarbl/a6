# README #
Ülesandes on telefonivõrk kui orienteerimata atsükliline kaaluta sidus graaf.
Tuleb välja selgitada graafi diameeter ehk pikim võimalik tee kahe antud sõlme vahel.

Allikad: 
https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
https://en.wikipedia.org/wiki/Longest_path_problem
https://stackoverflow.com/questions/64722638/finding-the-longest-path-in-an-unweighted-graph

## Command line examples. Näidete kasutamine käsurealt ##
#### Compilation. Kompileerimine: ####

```
#!bash

javac -cp src src/GraphTask.java
```

#### Execution. Käivitamine: ####

```
#!bash

java -cp src GraphTask
```


### Usage of tests. Testide kasutamine ###
#### Compilation of a test. Testi kompileerimine: ####

```
#!bash

javac -encoding utf8 -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' test/GraphTaskTest.java

```
In Windows replace colons by semicolons. Sama Windows aknas (koolonite asemel peavad olema semikoolonid):

```
#!bash

javac -encoding utf8 -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' test/GraphTaskTest.java


```

#### Running a test. Testi käivitamine: ####

```
#!bash

java -cp 'src:test:test/junit-4.13.1.jar:test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore GraphTaskTest
```

The same for Windows. Sama Windows aknas (koolonite asemel semikoolonid):

```
#!bash

java -cp 'src;test;test/junit-4.13.1.jar;test/hamcrest-core-1.3.jar' org.junit.runner.JUnitCore GraphTaskTest
```
